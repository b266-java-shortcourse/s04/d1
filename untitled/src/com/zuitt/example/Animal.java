package com.zuitt.example;

public class Animal {
//    properties
    private String name;
    private String color;

//    setter
    public void setName(String name){
        this.name = name;
    }
    public void setColor(String color){
        this.color = color;
    }

// getter
    public String getName(){
        return name;
    }
    public String getColor(){
        return color;
    }

// Constructor
    public Animal(){}
    public Animal(String name, String color){
        this.name = name;
        this.name = color;
    }
    //    methods
    public void call(){
        System.out.println("Hi my name is " + this.name);
    }


}
