package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        System.out.println("This car is driven by " + myCar.getDriverName());

        myCar.setName("Terry");
        myCar.setBrand("Ford");
        myCar.setYearOfMake(2023);
        System.out.println("Car Name: " + myCar.getName());
        System.out.println("Car Brand: " + myCar.getBrand());
        System.out.println("Car Year or Make: " + myCar.getYearOfMake());
        System.out.println("Car Driver: " + myCar.getDriverName());

        Dog myDog = new Dog();
        myDog.setName("Bucky");
        myDog.setColor("Black and White");

        myDog.speak();
        System.out.println(myDog.getName() + " " + myDog.getBreed() + " " + myDog.getColor());

        Dog oldPet = new Dog();
        oldPet.setName("Luchie");
        oldPet.setColor("Brown");

        oldPet.speak();
        System.out.println(oldPet.getName() + " " + oldPet.getBreed() + " " + oldPet.getColor());



    }


}
