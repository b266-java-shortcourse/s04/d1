package com.zuitt.example;

public class Car {
/* Object-Oriented Concepts

    Here are the definitions of the following:
        Object - An abstract idea in your mind that represents something in the real world
            Example: The concept of a dog
        Class - The representation of the object using code
            Example: Writing code that would describe a dog
        Instance - A unique copy of the idea, made "physical"
*/

/*
    Object
        are composed of two components

    1. States and attributes - what is the idea about?
    2. Behavior - what can idea do?

    Example: A person has attributes like name, age, height and weight. And a person can sleep, eat and speak.

 */

 /* Class Creation
        A class is composed of four parts:
        1. Properties - characteristics of the object
        2. Constructors - used to create an object
        3. Getters/Setter - get and set the values of each property of the object
        4. Methods - functions that an object can perform
 */

//  properties
    private String name;
    private String brand;
    private int yearOfMake;

//  Constructors
//  Empty Constructor

//  private Car(){}

//  Parameterized Constructor
    private Car(String name, String branh, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
    }

//   getters and setters

//   setters
    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake){
        this.yearOfMake = yearOfMake;
    }

//    getters

    public String getName(){
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getYearOfMake(){
        return this.yearOfMake;
    }

//  Methods
    public void drive(){
        System.out.println("The car is running.");
    }

/*  Access Modifiers
        1. default - no key word required
        2. private - only accessible within the class
        3. protected - only accessible to /with the classes (same package)
        4. public - can be accessed anywhere
*/

/* Fundamentals of OOP
    1. Encapsulation - mechanism of wrapping data (variables) and code acting on the data
    2. Inheritance - properties can be shared to subclasses
    3. Abstraction - way of using functions, process, codes without knowing its origin
    4. Polymorphism - passing of attribute and can be added or modified
 */


//   Make Driver a component of car
    private Driver d;
    public Car(){
        this.d = new Driver("Alejandro"); //whenever a new car is created, it will have a driver named Alejandro
    }

    public String getDriverName(){
        return this.d.getName();
    }


}
