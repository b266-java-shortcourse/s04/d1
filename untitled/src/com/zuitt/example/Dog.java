package com.zuitt.example;

public class Dog extends Animal {
//    property
    private String breed;


    public Dog(){
        super(); // animal() Constructor
        this.breed = "Chow Chow";
    }


    public Dog(String name, String color, String breed){
        super(name, color); //Animal(String name, string color) => constructor
        this.breed = breed;
    }

//    getter

    public String getBreed(){
        return this.breed;
    }

//    methods
    public void speak(){
        super.call(); //The call() method from Animal Class
        System.out.println("Woof woof");
    }
}
